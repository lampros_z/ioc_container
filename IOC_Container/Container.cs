﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace IOC_Container
{
    public class Container
    {
        private Dictionary<Type, Type> _map = new Dictionary<Type, Type>();

        public ContainerBuilder For<TSource>()
        {
            return For(typeof(TSource));
        }

        public ContainerBuilder For(Type SourceType)
        {
            return new ContainerBuilder(this, SourceType);
        }



        public TSource Resolve<TSource>()
        {
            return (TSource)Resolve(typeof(TSource));
        }

        public object Resolve(Type SourceType)
        {
            if(_map.ContainsKey(SourceType))
            {
                var destinationType = _map[SourceType];
                return CreateInstance(destinationType);
            }
            else if(SourceType.IsGenericType && _map.ContainsKey(SourceType.GetGenericTypeDefinition()))
            {
                var destinationType = _map[SourceType.GetGenericTypeDefinition()];
                var closedDestination = destinationType.MakeGenericType(SourceType.GenericTypeArguments);
                return CreateInstance(closedDestination);
            }
            else if(!SourceType.IsAbstract)
            {
                return CreateInstance(SourceType);
            }
            else
            {
                throw new InvalidOperationException("Could not resolve " + SourceType);
            }
        }

        private object CreateInstance(Type destinationType)
        {
            var parameters = destinationType.GetConstructors().OrderByDescending(c => c.GetParameters().Count())
                .First().GetParameters().Select(p => Resolve(p.ParameterType))
                .ToArray();
            return Activator.CreateInstance(destinationType, parameters);
        }

        public class ContainerBuilder
        {
            public ContainerBuilder(Container container, Type source)
            {
                _container = container;
                _sourceType = source;
            }

            public ContainerBuilder Use<TDestination>()
            {
                return Use(typeof(TDestination));
            }
            public ContainerBuilder Use(Type DestinationType)
            {
                _container._map.Add(_sourceType, DestinationType);
                return this;
            }
            Container _container;
            Type _sourceType;


        }
    }
}
