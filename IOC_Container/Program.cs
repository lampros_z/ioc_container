﻿using System;
using System.Collections.Generic;

namespace IOC_Container
{
    class Program
    {
        static Action<string> print = s => Console.WriteLine(s);
        public static void Main(string[] args)
        {
            
            //SortedList<,> unbound generic -> cannot be instantiated
            var employeeList = CreateCollection(typeof(List<>), typeof(Employee));
            //List'1[Employee] closed type
            Console.Write(employeeList.GetType().Name);
            var genericArgs = employeeList.GetType().GenericTypeArguments;
            foreach(var arg in genericArgs)
            {
                Console.Write("[{0}]", arg.Name);
            }
            print("");
            var employee = new Employee();
            var employeeType = typeof(Employee);
            var methodInfo = employeeType.GetMethod("Speak");
            methodInfo = methodInfo.MakeGenericMethod(typeof(DateTime));
            methodInfo.Invoke(employee, null);
        }

        private static object Create(Type type)
        {
            return Activator.CreateInstance(type);
        }

        private static object CreateCollection(Type CollectionType, Type ItemType)
        {
            var ClosedType = CollectionType.MakeGenericType(ItemType);
            return Activator.CreateInstance(ClosedType);
        }

        public class Employee
        {
            public string Name { get; set; }
            public void Speak<T>()
            {
                print(typeof(T).Name);
            }
        }
    }
}
